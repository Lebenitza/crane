FROM gcr.io/go-containerregistry/crane:debug

# Because of pre_clone_script & pre_build_script in
# GitLab runner using #!/bin/sh shebang
RUN ["/busybox/sh", "-c", "mkdir -p /bin && ln -s /busybox/sh /bin/sh"]